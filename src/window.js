/* window.js
 *
 * Copyright 2019 Unknown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk } = imports.gi;

var mainWindow = GObject.registerClass({
    GTypeName: 'mainWindow',
    Template: 'resource:///gq/skyenet/flighttrackr/window.ui',
    InternalChildren: [ 'mainNotebook',
                        'headerBar',
                        'savedFlightsBtn',
                        'searchFlightsBtn',
                        'flightMapBtn'
                        ]
},
class mainWindow extends Gtk.ApplicationWindow {
    _init(application) {
        super._init({ application });

        this.savedFlightsBtn.connect("clicked", this.mainNotebook.page = "0");
        this.searchFlightsBtn.connect("clicked", this.mainNotebook.page = "1");
        this.flightMapBtn.connect("clicked", this.mainNotebook.page = "2");
    }
});


